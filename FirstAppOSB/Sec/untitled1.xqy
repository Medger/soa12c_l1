xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://TargetNamespace.com/RestService_Operation1_response";
(:: import schema at "Resources/nxsd_schema1.xsd" ::)
declare namespace ns2="http://xmlns.oracle.com/FirstAppOSB/Sec/RestService";
(:: import schema at "Resources/RestService.wsdl" ::)

declare variable $valor as element() (:: schema-element(ns2:Operation1_params) ::) external;

declare function local:func($valor as element() (:: schema-element(ns2:Operation1_params) ::)) as element() (:: schema-element(ns1:Root-Element) ::) {
    <ns1:Root-Element>
        <ns1:teste>{fn:data($valor/ns2:param1)}</ns1:teste>
    </ns1:Root-Element>
};

local:func($valor)
