xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://TargetNamespace.com/ConsumerTest_Operation1_request";

declare namespace ns2="http://xmlns.oracle.com/FirstAppOSB/ConsumerAPI/FirstAPI";


declare variable $rq as element()  external;

declare function local:func($rq as element() ) as element()  {
    <ns2:opSum_params>
        <ns2:value1>{fn:data($rq/ns1:value1)}</ns2:value1>
        <ns2:value2>{fn:data($rq/ns1:value2)}</ns2:value2>
    </ns2:opSum_params>
};

local:func($rq)
