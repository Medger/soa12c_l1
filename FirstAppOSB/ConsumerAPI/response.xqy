xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://TargetNamespace.com/Calculate_sum_op_response";
(:: import schema at "FirstAPI/Resources/xsd_response_sum_op.xsd" ::)
declare namespace ns2="http://TargetNamespace.com/ConsumerTest_Operation1_response";
(:: import schema at "Resources/nxsd_schema1.xsd" ::)

declare variable $rs as element() (:: schema-element(ns1:Root-Element) ::) external;
declare variable $name as xs:string external;

declare function local:func($rs as element() (:: schema-element(ns1:Root-Element) ::),
        $name as xs:string)  as element() (:: schema-element(ns2:Root-Element) ::) {
    <ns2:Root-Element>
        <ns2:name>{$name}</ns2:name>
        <ns2:resultOfSum>{fn:data($rs/ns1:result)}</ns2:resultOfSum>
    </ns2:Root-Element>
};

local:func($rs, $name)
